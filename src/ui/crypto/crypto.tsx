import { ChangeEvent, FC, memo, ReactElement, useState } from "react";
import * as NodeRSA from "node-rsa";

const PRIVATE_KEY = import.meta.env.VITE_RSA_PRIVATE_KEY;
const RSA_PKCS1_PADDING = 1;

const Crypto: FC = (): ReactElement => {
  const [decryped, setDecrypted] = useState("");
  const [inputText, setInputText] = useState("");

  const handleChange = ({ target }: ChangeEvent<HTMLTextAreaElement>) => {
    setInputText(target.value);
  };
  const onClickDecrypt = () => {
    setDecrypted("");
    const key = new NodeRSA(PRIVATE_KEY);

    key.setOptions({
      encryptionScheme: {
        scheme: "pkcs1",
        padding: RSA_PKCS1_PADDING,
      },
    });

    const decryptedText = key.decrypt(inputText, "utf8");
    setDecrypted(decryptedText);
  };

  return (
    <div>
      <textarea
        cols={50}
        rows={5}
        onChange={handleChange}
        value={inputText}
      ></textarea>
      <div>
        <button type="button" onClick={onClickDecrypt} disabled={!inputText}>
          Decrypt
        </button>
      </div>
      <div>
        <pre>{decryped}</pre>
      </div>
    </div>
  );
};

export default memo(Crypto);
